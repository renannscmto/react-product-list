import React from "react";
import Item from "./Items";

export default function ProductsTable({products, isStocks}) {
    const rows = [];
    products.map(item => {
        if(rows.includes(item.category)) return;
        rows.push(item.category);
    })

    return (
        <table>
            <thead>
              <tr>
                <th>Name</th>
                <th>Price</th>
              </tr>
            </thead>

            <tbody>
                { 
                    rows.map((category) => {
                        return <Item category={category} products={products} isStocks={isStocks}/>
                    })
                } 
            </tbody>
    </table>
    )
}
