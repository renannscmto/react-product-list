import React, {useState} from "react";

import products from "../backend/products.js";
import SearchBar from "./SearchBar.jsx";
import ProductsTable from "./ProductsTable.jsx";

export default function Table() {
    const [isStocks, setIsStoke] = useState(true);

    const hendleShowStock = () => {
        setIsStoke(!isStocks)
    }

    return (
        <div className="container-table">
            <SearchBar hendleShowStock={hendleShowStock} isStocks={isStocks}/>
            <ProductsTable isStocks={isStocks} products={products} />
        </div>
    )
}