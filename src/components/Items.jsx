import React from "react";

export default function Item({category, products, isStocks}) {
    return (
        <div>
            <h1>{category}</h1>
            {
                isStocks ? 
                products.filter(item => item.category === category)
                        .map((product) => {
                            return (
                                <ul key={product.id}> 
                                    <li> {product.name} </li>
                                    <li> {product.price} </li>
                                </ul>  
                            )
                        })
                : 
                products.filter(item => item.category === category)
                        .filter(item => item.stocked === true)
                        .map((product) => {
                            return (
                                <ul key={product.id}> 
                                    <li> {product.name} </li>
                                    <li> {product.price} </li>
                                </ul>  
                            )
                        })
            }
        </div>
    )
}