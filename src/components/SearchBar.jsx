import React from "react";

export default function SearchBar({hendleShowStock, isStocks}) {
    return (
        <div className="container-searchbar">
            <input name="search" type="text" placeholder="Search..." />

            <div className="checkbox-container">
                <input name="check" type="checkbox" checked={isStocks} onChange={hendleShowStock}/>
                <p>Show all products! </p>
            </div>
        </div>
    )
}